/*
    Copyright (C) 2011 Laszlo Kajan, Technical University of Munich, Germany

    This file is part of librostlab.

    librostlab is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ROSTLAB_BLAST_PARSER_DRIVER_H
#define ROSTLAB_BLAST_PARSER_DRIVER_H 1

#include <stdio.h>
#include <string>
#include <map>
#include <rostlab/rostlab_stdexcept.h>
#include "rostlab/blast-parser-parser.h"
#include "rostlab/blast-result.h"

// Tell Flex the lexer's prototype ...
#define YY_DECL                                             \
  rostlab::blast::parser::token_type                        \
  yylex( rostlab::blast::parser::semantic_type* __yylval,   \
         rostlab::blast::parser::location_type* __yylloc,   \
         rostlab::blast::parser_driver& __drv,              \
         void* yyscanner )
// ... and declare it for the parser's sake.
YY_DECL;
#define YY_DECL_FRIEND                                      \
  rostlab::blast::parser::token_type                        \
  ::yylex( rostlab::blast::parser::semantic_type* __yylval, \
         rostlab::blast::parser::location_type* __yylloc,   \
         rostlab::blast::parser_driver& __drv,              \
         void* yyscanner )

namespace rostlab {

namespace blast {

class parser_error : public rostlab::runtime_error
{
  public:
                  parser_error( const std::string& __msg ) : rostlab::runtime_error(__msg){}
};

/// Blast (default, -m 0) output parser.
/** Example:
    \include parseblast.cpp
*/
class parser_driver {
    friend class rostlab::blast::parser;
    friend YY_DECL_FRIEND;
  public:
    typedef       rostlab::blast::result    result_type;
  private:
    std::string   _istream_name; // a name for error reporting
    FILE*         _istream;
    result_type   _result;
    void*         _scanner; // yyscan_t
  private:
    // this is a resource - disable copy contructor and copy assignment
                  parser_driver( const parser_driver& ){};
    parser_driver& operator=(const parser_driver&){return *this;};

    std::string   _buffer;
    int           _n1, _n2;
    void          _scan_init();
    void          _scan_destroy();
  public:
    parser_driver( FILE* __istream = stdin, const std::string& __istream_name = "stdin" ) : _istream_name(__istream_name), _istream(__istream)
    {
      _scan_init();
    }
    virtual ~parser_driver()
    {
      _scan_destroy();
    }

    /// Parse one result from the input stream.
    /** Each call returns a reference to the filled result structure.  The structure may be empty in case there are no more results in the stream. */
    const result_type&
                  parse( bool __trace_parsing = false, bool __trace_scanning = false ) throw (rostlab::blast::parser_error);

    /// Get tracing of scanning.
    bool          trace_scanning();
    /// Set tracing of scanning.
    void          trace_scanning( bool __b );

    /// Print error message for given location.
    void          error( const rostlab::blast::location& __loc, const std::string __msg )
    {
      std::cerr << __loc << ": " << __msg << "\n";
    }

    /// Print error message.
    void          error( const std::string __msg )
    {
      std::cerr << __msg << "\n";
    }

    /// Read-only access to results.
    /** This method provides access to the last parsed results or an empty result structure if parse() was not yet called. */
    const result_type&
                  result() const { return _result; }
};

} // namespace blast

} // namespace rostlab

#endif // ROSTLAB_BLAST_PARSER_DRIVER_H

// vim:et:ts=4:ai:
