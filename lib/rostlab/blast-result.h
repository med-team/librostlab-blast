/*
    Copyright (C) 2011 Laszlo Kajan, Technical University of Munich, Germany

    This file is part of librostlab.

    librostlab is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef ROSTLAB_BLAST_RESULT_H
#define ROSTLAB_BLAST_RESULT_H
#include <stdint.h>
#include <sstream>
#include <string>
#include <vector>
#include <rostlab/aux_functions.h>

namespace rostlab {

namespace blast {

/// Data specific to an iterated blast round.
struct round {
    /// Index of first one-line description of this round in vector of all one-line descriptions.
    size_t        oneline_idx;
    /// Count of one-line descriptions in round.
    size_t        oneline_cnt;
    /// Index of first hit of this round in vector of all hits.
    size_t        hit_idx;
    /// Count of hits in round.
    size_t        hit_cnt;
    /// Index of first one-line description of sequences not found previously in this round. noidx if there is no such, i.e. in case there are no new sequences in this round.
    size_t        oneline_new_idx;
    /// Count of one-line descriptions for sequences not found previously.
    size_t        oneline_new_cnt;
    static const size_t     noidx = static_cast<size_t>(-1);
  public:
                  round( size_t __oneline_idx = 0, size_t __oneline_cnt = 0, size_t __hit_idx = 0, size_t __hit_cnt = 0, size_t __oneline_new_idx = noidx, size_t __oneline_new_cnt = 0 ) : oneline_idx( __oneline_idx), oneline_cnt(__oneline_cnt), hit_idx( __hit_idx ), hit_cnt(__hit_cnt), oneline_new_idx(__oneline_new_idx), oneline_new_cnt(__oneline_new_cnt){}
    virtual       ~round(){}
};

/// High-scoring segment pair.
/** Depending on the type of blast results, fields may be unused. */
struct hsp {
    /// An collection of constants that specify all permissible modes of composition adjustment.
    /** Copied from ncbi-tools6-6.1.20090809/algo/blast/composition_adjustment/composition_constants.h in order to avoid having to depend on that library only for this.  */
    typedef enum ECompoAdjustModes {
        eNoCompositionBasedStats       = 0,
        eCompositionBasedStats         = 1,
        eCompositionMatrixAdjust       = 2,
        eCompoForceFullMatrixAdjust    = 3,
        eNumCompoAdjustModes
    } ECompoAdjustModes; // ncbi-tools6-6.1.20090809/algo/blast/composition_adjustment/composition_constants.h:51 // a copy is made so we do not have to depend on that library only for this
  public:
    double        bit_score;
    size_t        raw_score;
    double        e_value;
    ECompoAdjustModes   method;
    size_t        identities;
    size_t        positives;
    size_t        gaps;
    /// Query strand [Plus|Minus].
    std::string         q_strand;
    /// Subject strand [Plus|Minus].
    std::string         s_strand;
    /// Query frame.
    /** Initialized to 32.*/
    int8_t        q_frame;
    /// Subject frame.
    /** Initialized to 32. */
    int8_t        s_frame;
    /// Query start (1-based).
    size_t        q_start;
    /// Query alignment string.
    std::string         q_ali;
    /// Query end (1-based).
    size_t        q_end;
    /// Match line.
    std::string         match_line;
    /// Subject start (1-based).
    size_t        s_start;
    /// Subject alignment string.
    std::string         s_ali;
    /// Subject end (1-based).
    size_t        s_end;
  public:
                  hsp( double __bit_score = 0, size_t __raw_score = 0 ) : bit_score(__bit_score), raw_score(__raw_score), e_value(0), method(eNoCompositionBasedStats), identities(0), positives(0), gaps(0), q_frame(32), s_frame(32),
                    q_start(0), q_end(0), s_start(0), s_end(0){}
    virtual       ~hsp(){}

    /// Translate method code to string.
    /** The trailing `.' is not included.
     *  eCompositionBasedStats => "Composition-based stats"
     *  eCompositionMatrixAdjust => "Compositional matrix adjust"
     *
     *  Default: integer code of enum value.
     * */
    inline static std::string
                    methodstr( const ECompoAdjustModes __m )
    {
        switch( __m )
        {
            case eCompositionBasedStats:
                return "Composition-based stats"; break;
            case eCompositionMatrixAdjust:
                return "Compositional matrix adjust"; break;
            default:
                std::stringstream ss; ss << __m; return ss.str();
        }
    }
    /// Translate method description to mode code.
    /** E.g. `Composition-based stats' => eCompositionBasedStats. The trailing dot - if present - is ignored. */
    inline static ECompoAdjustModes
                    methfromstr( std::string __m )
    {
        if( __m.size() > 0 && __m[ __m.size()-1 ] == '.' ) __m.resize( __m.size()-1 );
        //           1
        // 012345678901
        // Composition-based stats
        // Compositional matrix adjust
        if( __m.size() >= 12 )
        {
            if( __m[11] == '-' ) return eCompositionBasedStats;
            if( __m[11] == 'a' ) return eCompositionMatrixAdjust;
        }
        return eNoCompositionBasedStats;
    }
};

/// Blast hit.
/** A hit consists of one or more hsps. */
struct hit {
    std::string         name;
    std::string         desc;
    /// Full length of subject sequence.
    size_t              length;
    std::vector<hsp>    hsps;
  public:
                  hit( const std::string& __name = "", const std::string& __desc = "", size_t __length = 0 ) : name(__name), desc(__desc), length(__length) {}
    virtual       ~hit(){}
};

/// One-line description.
struct oneline {
    std::string     name;
    std::string     desc;
    /// Bit score.
    double          bit_score;
    double          e_value;
                    oneline( const std::string& __name = "", const std::string& __desc = "", double __bit_score = 0, double __e_value = 0 ) : name(__name), desc(__desc), bit_score(__bit_score), e_value(__e_value){}
                    oneline( const hit& __h ) : name(__h.name), desc(__h.desc), bit_score(__h.hsps.at(0).bit_score), e_value(__h.hsps.at(0).e_value){}
    virtual         ~oneline(){}
};

/// Blast result for one query.
struct result {
    bool            empty;
    std::string     blast_version;
    std::vector<std::string>
                    references;
    /// Vector of iterated blast round information.
    std::vector<rostlab::blast::round>
                    rounds;
    /// Query name.
    std::string     q_name;
    /// Query description.
    std::string     q_desc;
    /// Query length.
    size_t          q_length;
    /// Database name.
    std::string     db_name;
    /// Number of sequences in database.
    size_t          db_nseq;
    /// Number of letters in database.
    size_t          db_nletter;
    /// Vector of all one-line descriptions.
    std::vector<rostlab::blast::oneline>
                    onelines;
    /// Indicates that the search has converged.
    bool            converged;
    /// Vector of all hits.
    std::vector<rostlab::blast::hit>
                    hits;
    /// Tail part of blast result as a long string.
    std::string     tail;
  public:
                    result() : empty(true), q_length(0), db_nseq(0), db_nletter(0), converged(false) {}
    virtual         ~result(){}

    /// Conversion to bool - true when not empty.
    /** This allows writing 'while( res = parser_driver.parse() ){ ... }'. */
    operator bool() const { return !empty; }
};

} // namespace blast

/// Stream output operator for blast::round.
inline
std::ostream&     operator<<( std::ostream& __os, const rostlab::blast::round& __r )
{
  __os << "ol_idx = " << __r.oneline_idx << ", ol_cnt = " << __r.oneline_cnt << ", hit_idx = " << __r.hit_idx << ", hit_cnt = " << __r.hit_cnt << ", ol_new_idx = " << __r.oneline_new_idx << ", ol_new_cnt = " << __r.oneline_new_cnt;
  return __os;
}

/// Stream output operator for blast::oneline.
inline
std::ostream&     operator<<( std::ostream& __os, const rostlab::blast::oneline& __r )
{
  __os << "n = " << __r.name << " d = " << __r.desc << ": " << __r.bit_score << " bits, " << __r.e_value << " E";
  return __os;
}

/// Stream output operator for blast::hsp.
inline
std::ostream&     operator<<( std::ostream& __os, const rostlab::blast::hsp& __r )
{
  __os << "bits = " << __r.bit_score << ", raw = " << __r.raw_score << ", E = " << __r.e_value << ", method = " << blast::hsp::methodstr(__r.method) << ", ident = " << __r.identities <<
    ", pos = " << __r.positives << ", gaps = " << __r.gaps << ", q_strand = " << __r.q_strand << ", s_strand = " << __r.s_strand << ", q_frame = " << (int)__r.q_frame <<
    ", s_frame = " << (int)__r.s_frame << ", q_start = " << __r.q_start << ", q_ali = " << __r.q_ali << ", q_end = " << __r.q_end << ", match_line = " << __r.match_line <<
    ", s_start = " << __r.s_start << ", s_ali = " << __r.s_ali << ", s_end = " << __r.s_end;
  return __os;
}

/// Stream output operator for blast::hit.
inline
std::ostream&     operator<<( std::ostream& __os, const rostlab::blast::hit& __r )
{
  __os << "n = " << __r.name << " d = " << __r.desc << " Length = " << __r.length << " " << __r.hsps;
  return __os;
}

/// Stream output operator for blast::result.
inline
std::ostream&     operator<<( std::ostream& __os, const rostlab::blast::result& __r )
{
  __os << __r.blast_version << "\n\nreferences: " << __r.references << "\n\nrounds: " << __r.rounds << "\n\nn = " << __r.q_name << " d = " << __r.q_desc << " (" << __r.q_length <<
    " letters)\n\nDatabase: " << __r.db_name << " " << __r.db_nseq << " sequences; " << __r.db_nletter << " total letters\n\none-line desc: " << __r.onelines << "\n\n" <<
    ( __r.converged ? "CONVERGED!\n\n" : "" ) << "hits: " << __r.hits << "\n\n" << __r.tail;
  return __os;
}

} // namespace rostlab

#endif // ROSTLAB_BLAST_RESULT_H
// vim:et:ts=4:ai:
