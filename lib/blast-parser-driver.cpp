/*
    Copyright (C) 2011 Laszlo Kajan, Technical University of Munich, Germany

    This file is part of librostlab.

    librostlab is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "rostlab/blast-parser-driver.h"

const rostlab::blast::parser_driver::result_type&
                  rostlab::blast::parser_driver::parse( bool __trace_parsing, bool __trace_scanning ) throw (rostlab::blast::parser_error)
{
  rostlab::blast::parser parser(*this, _scanner);
  parser.set_debug_level( __trace_parsing );
  trace_scanning( __trace_scanning );

  // make sure we parse into a fresh result structure
  _result = result_type();

  int res = parser.parse();
  if( res != 0 ) throw rostlab::blast::parser_error("parser error");

  return _result;
}

// vim:et:ts=4:ai:
