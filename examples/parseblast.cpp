#include <iostream>
#include <rostlab/blast-parser-driver.h>

using namespace rostlab;

int main()
{
	rostlab::blast::parser_driver p(stdin);

	while( const rostlab::blast::result& res = p.parse() )
		std::cout << res;

	return 0;
}

// Compile with:
// g++ -Wall -lrostlab-blast parseblast.cpp -o parseblast -L../lib/.libs
