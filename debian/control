Source: librostlab-blast
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Laszlo Kajan <lkajan@rostlab.org>,
           Andreas Tille <tille@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               d-shlibs,
               bison,
               doxygen,
               flex,
               graphviz,
               texlive-latex-recommended,
               librostlab-dev,
               texlive-fonts-recommended,
               texlive-latex-extra
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/librostlab-blast
Vcs-Git: https://salsa.debian.org/med-team/librostlab-blast.git
Homepage: https://rostlab.org/
Rules-Requires-Root: no

Package: librostlab-blast0v5
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Conflicts: librostlab-blast0
Description: very fast C++ library for parsing the output of NCBI BLAST programs
 This package provides a very fast library for parsing the default output of
 NCBI BLAST programs into a C++ structure.
 .
 libzerg is faster, but it provides only lexing (i.e. it only returns pairs
 of token identifiers and token string values).  librostlab-blast uses a
 parser generated with bison on top of a flex-generated lexer very similar to
 that of libzerg.
 .
 This package contains the shared library.

Package: librostlab-blast0-dev
Architecture: any
Section: libdevel
Depends: librostlab-blast0v5 (= ${binary:Version}),
         librostlab-dev,
         ${misc:Depends}
Suggests: librostlab-blast-doc
Conflicts: librostlab-blast-dev
Provides: librostlab-blast-dev
Description: very fast C++ library for parsing the output of NCBI BLAST programs (devel)
 This package provides a very fast library for parsing the default output of
 NCBI BLAST programs into a C++ structure.
 .
 libzerg is faster, but it provides only lexing (i.e. it only returns pairs
 of token identifiers and token string values).  librostlab-blast uses a
 parser generated with bison on top of a flex-generated lexer very similar to
 that of libzerg.
 .
 This package contains files necessary for developing applications with
 librostlab-blast.

Package: librostlab-blast-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libjs-jquery
Recommends: librostlab-doc
Description: very fast C++ library for parsing the output of NCBI BLAST programs (doc)
 This package provides a very fast library for parsing the default output of
 NCBI BLAST programs into a C++ structure.
 .
 libzerg is faster, but it provides only lexing (i.e. it only returns pairs
 of token identifiers and token string values).  librostlab-blast uses a
 parser generated with bison on top of a flex-generated lexer very similar to
 that of libzerg.
 .
 This package contains html and pdf documentation.
